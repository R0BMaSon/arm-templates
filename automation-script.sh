echo "Enter the Resource Group name:" &&
read resourceGroupName &&
echo "Enter the foldername:" &&
read foldername &&
echo "Enter the location (i.e. centralus):" &&
read location &&
# echo "Enter the project name (used for generating resource names):" &&
# read projectName &&
# echo "Enter the administrator username:" &&
# read username &&
# echo "Enter the SSH public key:" &&
# read key &&
az group create --name $resourceGroupName --location "$location" &&
az group deployment create --resource-group $resourceGroupName --template-file $foldername/azure-deploy.json --parameters $foldername/azure-deploy.parameters.json
# az vm show --resource-group $resourceGroupName --name "$projectName-vm" --show-details --query publicIps --output tsv

# az group deployment create --resource-group $resourceGroupName --template-file nsg/azure-deploy.json

